{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div>\n",
    "    <img src=\"figures/slew_logo.png\" style=\"float: right;height: 15em;\">\n",
    "</div>\n",
    "\n",
    "# SLEW Case 1 - Electromagnetic Phenomena of Synchronous Machines\n",
    "\n",
    "## Theoretical background\n",
    "\n",
    "### The generator and its windings\n",
    "\n",
    "<img src=\"figures/SubtransientTransientSteadyMachine.svg\" width=\"600\" align=\"left\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The winding resistance (of armature i.e. stator) dissipates energy at rate proportional to the current squared. The DC components (part of total phase currents) decay ($T_a$). Those armature currents induce direct currents in the rotor windings. They decay with respect to time constant determined by the particular rotor circuit in which they flow:\n",
    "  - DC component of the damper winding current decays with the d-axis subtransient short-circuit time constant $T''_d$\n",
    "  - DC component of the field current decays with the field winding time constant, called the d-axis transient short circuit time constant $T'_d$\n",
    "  - Resistance of damper windings is much higher than the resistance of the field winding $T''_d \\leq T'_d$  \n",
    "*\\[text and figure adopted from J. Machowski, Z. Lubosny, J. Bialek, and J. R. Bumby, Power system dynamics: stability and control\\]*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Case description"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A round-rotor machine has the following values:\n",
    "\n",
    "| Parameter       | Value      | Description                            |\n",
    "|-----------------|------------|----------------------------------------|\n",
    "| $$X_{fd}$$      | 0.1648 p.u.    | Reactance of field winding             |\n",
    "| $$X_l$$         | 0.15 p.u.      | Armature leakage reactance             |\n",
    "| $$X_d$$         | 1.8099 p.u.    | Direct-axis synchronous reactance      |\n",
    "| $$X_q$$         | 1.76   p.u.    | Quadrature-axis synchronous reactance  |\n",
    "| $$T'_{d_0}$$    | 8.0669 s   | Direct-axis transient open-circuit time constant    |\n",
    "| $$T''_{d_0}$$   | 0.03 s     | Direct-axis subtransient open-circuit time constant |\n",
    "| $$T'_{q_0}$$    | 0.9991 s   | Quadrature-axis transient open-circuit time constant|\n",
    "| $$T''_{q_0}$$   | 0.07 s     | Quadrature-axis subtransient open-circuit time constant |\n",
    "| $$X''_d$$       | 0.2299 p.u.    | Direct-axis subtransient reactance     |\n",
    "| $$X'_q$$        | 0.65  p.u.     | Quadrature-axis transient reactance    |\n",
    "| $$X''_q$$       | 0.25  p.u.     | Quadrature-axis subtransient reactance |\n",
    "| $$f_n$$         | 60 Hz      | Nominal frequency                     |\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Case tasks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Task 1 \n",
    "Calculate $X'_d$, $R_{fd}$, $X_{Dd}$, and $R_{Dd}$ from the given quantities.\n",
    "\n",
    "#### Task 2\n",
    "Apply $X'_d$ by adjusting *\"Ld_t\"* in SLEW and generate the corresponding short-circuit current plot.\n",
    "\n",
    "#### Task 3\n",
    "The open circuit time constant $T'_{d0}$ shall be increased to $18s$.\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;a) Find the corresponding value of $R_{fd}$ that leads to that increase.\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;b) Apply the new $T'_{d0}$ by adjusting *\"Td0_t\"* in SLEW, run the simulation, and observe the impact of the increase.\n",
    "\n",
    "#### Task 4\n",
    "The subtransient open circuit time constant $T''_{d0}$ shall be increased to $0.09s$.\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;a) Find the corresponding value of $R_{Dd}$ that leads to that increase.\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;b) Reset $T'_{d0}$ to its original value (see Case description) and apply the new $T''_{d0}$ instead by adjusting *\"Td0_s\"* in SLEW, run the simulation, and observe the impact of such increase.\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**You can check your calculation solutions and process the results from SLEW using the prepared notebook cells below.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Case solutions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Setup your Python for post-processing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Import relevant Python packages by executing the following cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import display\n",
    "from villas.web.result import Result\n",
    "from pprint import pprint\n",
    "import tempfile\n",
    "import os\n",
    "from villas.dataprocessing.readtools import *\n",
    "from villas.dataprocessing.timeseries import *\n",
    "import matplotlib.pyplot as plt\n",
    "import scipy.io as sio\n",
    "outputs = sio.loadmat('outputs/case1_output.mat', simplify_cells=True)\n",
    "\n",
    "\n",
    "%matplotlib widget"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "#### Task 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Calculate $X'_d$, $R_{fd}$, $X_{Dd}$ and $R_{Dd}$ from the given quantities."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To check your results, enter your solution for $X'_d$ rounded to 4 digits behind the comma:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Xd_t=input('Xd_t:')\n",
    "print('Your result is', 'correct!' if round(float(Xd_t),4)==round(outputs['Xd_t'],4) else 'incorrect. You should double-check your calculations.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To check your results, enter your solution for $R_{fd}$ rounded to 4 digits behind the comma:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Rfd=input('Rfd:')\n",
    "print('Your result is', 'correct!' if round(float(Rfd),4)==round(outputs['Rfd'],4) else 'incorrect. You should double-check your calculations.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To check your results, enter your solution for $X_{Dd}$ rounded to 4 digits behind the comma:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "XDd=input('XDd:')\n",
    "print('Your result is', 'correct!' if round(float(XDd),4)==round(outputs['XDd'],4) else 'incorrect. You should double-check your calculations.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To check your results, enter your solution for $R_{Dd}$ rounded to 4 digits behind the comma:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "RDd=input('RDd:')\n",
    "print('Your result is', 'correct!' if round(float(RDd),4)==round(outputs['RDd'],4) else 'incorrect. You should double-check your calculations.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Task 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Apply $X'_d$ by adjusting *\"Ld_t\"* in SLEW and generate the corresponding short-circuit current plot."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Enter SLEW under [https://slew.k8s.eonerc.rwth-aachen.de](https://slew.k8s.eonerc.rwth-aachen.de).  \n",
    "Adjust the parameter *\"Ld_t\"* and run a corresponding simulation (*\"Ld_t\"* has the same per-unit value as $X'_d$). Further instructions on how to handle SLEW you can find in the [SLEW Platform Tutorial](./SLEW_Platform_Tutorial.ipynb).  \n",
    "Then, enter your code snippet for result download in the following cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Access result using token\n",
    "r = Result(1, 'xyz', endpoint='https://slew.k8s.eonerc.rwth-aachen.de')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot the results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results_file_name='EMT_SynGenDQ7odTrapez_OperationalParams_SMIB_Fault_JsonSyngenParams.csv'\n",
    "\n",
    "# Get file by name\n",
    "f = r.get_file_by_name(results_file_name)\n",
    "\n",
    "# Create temp dir\n",
    "tmpdir = tempfile.mkdtemp()\n",
    "results_file_path = os.path.join(tmpdir,results_file_name)\n",
    "\n",
    "# Load files as bytes\n",
    "f = f.download(dest=results_file_path)\n",
    "\n",
    "# Get time series object from csv\n",
    "ts_res1 = read_timeseries_csv(results_file_path, print_status=False)\n",
    "\n",
    "# Plot the currents\n",
    "plt.figure(figsize=(8,12))\n",
    "name_list = ['i_gen_0', 'i_gen_1', 'i_gen_2']\n",
    "phases = ['a', 'b', 'c']\n",
    "colors = ['C0', 'C1', 'C2']\n",
    "for name in name_list:\n",
    "    i = name_list.index(name)\n",
    "    plt.subplot(3,1,1+i)\n",
    "    plt.ylabel(\"current (kA)\")\n",
    "    plt.xlabel(\"time (s)\")\n",
    "    plt.plot(ts_res1[name].time, ts_res1[name].values/1e3, label='SG task 2 (phase '+phases[i]+')', color=colors[i])\n",
    "    plt.xlim([0, 1])\n",
    "    plt.legend(loc='upper right')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Task 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " The open circuit time constant $T'_{do}$ shall be increased to $18s$  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "####  a) \n",
    "Find the corresponding value of $R_{fd}$ that leads to that increase."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To check your results, enter your solution for $R_{fd}$ rounded to 7 digits behind the comma:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "Rfd_T3a=input('Rfd in Subtask 3:')\n",
    "print('Your result is', 'correct!' if round(float(Rfd_T3a),7)==round(outputs['Rfd_T3a'],7) else 'incorrect. You should double-check your calculations.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### b) \n",
    "Apply the new $T'_{do}$ by adjusting *\"Td0_t\"* in SLEW, run the simulation and observe the impact of such increase.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Enter SLEW under [https://slew.k8s.eonerc.rwth-aachen.de](https://slew.k8s.eonerc.rwth-aachen.de).   \n",
    "Adjust the parameter *\"Td0_t\"* and run a corresponding simulation. Further instructions on how to handle SLEW you can find in the [SLEW Platform Tutorial](./SLEW_Platform_Tutorial.ipynb).  \n",
    "Then, enter your code snippet for result download in the following cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Access result using token\n",
    "r = Result(1, 'xyz', endpoint='https://slew.k8s.eonerc.rwth-aachen.de')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot the results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "results_file_name='EMT_SynGenDQ7odTrapez_OperationalParams_SMIB_Fault_JsonSyngenParams.csv'\n",
    "\n",
    "# Get file by name\n",
    "f = r.get_file_by_name(results_file_name)\n",
    "\n",
    "# Create temp dir\n",
    "tmpdir = tempfile.mkdtemp()\n",
    "results_file_path = os.path.join(tmpdir,results_file_name)\n",
    "\n",
    "# Load files as bytes\n",
    "f = f.download(dest=results_file_path)\n",
    "\n",
    "# Get time series object from csv\n",
    "ts_res2 = read_timeseries_csv(results_file_path, print_status=False)\n",
    "\n",
    "# Plot the currents\n",
    "plt.figure(figsize=(8,12))\n",
    "name_list = ['i_gen_0', 'i_gen_1', 'i_gen_2']\n",
    "phases = ['a', 'b', 'c']\n",
    "colors = ['C0', 'C1', 'C2']\n",
    "for name in name_list:\n",
    "    i = name_list.index(name)\n",
    "    plt.subplot(3,1,1+i)\n",
    "    plt.ylabel(\"current (kA)\")\n",
    "    plt.xlabel(\"time (s)\")\n",
    "#     plt.plot(ts_res1[name].time, ts_res1[name].values/1e3, label='SG task 2 (phase '+phases[i]+')', color=colors[i])\n",
    "#     plt.plot(ts_res2[name].time, ts_res2[name].values/1e3, label='SG task 3 (phase '+phases[i]+')', color='black', linestyle=':')\n",
    "    plt.plot(ts_res2[name].time, ts_res2[name].values/1e3, label='SG task 3 (phase '+phases[i]+')', color=colors[i])\n",
    "    plt.plot(ts_res1[name].time, ts_res1[name].values/1e3, label='SG task 2 (phase '+phases[i]+')', color='0.8')\n",
    "    \n",
    "    \n",
    "    plt.xlim([0, 1])\n",
    "    plt.legend(loc='upper right')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Task 4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The open circuit time constant $T''_{do}$ shall be increased to $0.09s$  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### a) \n",
    "Find the corresponding value of $R_{Dd}$ that leads to that increase.\n",
    "\n",
    "To check your results, enter your solution for $R_{Dd}$ rounded to 4 digits behind the comma:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "RDd_T4a=input('RDd in Subtask 4:')\n",
    "print('Your result is', 'correct!' if round(float(RDd_T4a),4)==round(outputs['RDd_T4a'],4) else 'incorrect. You should double-check your calculations.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### b) \n",
    "Reset $T'_{do}$ to its original value and apply the new $T''_{do}$ instead by adjusting *\"Td0_s\"* in SLEW, run the simulation and observe the impact of such increase.\n",
    "\n",
    "Enter SLEW under [https://slew.k8s.eonerc.rwth-aachen.de](https://slew.k8s.eonerc.rwth-aachen.de).  \n",
    "Adjust the parameter *\"Td0_s\"* and run a corresponding simulation. Further instructions on how to handle SLEW you can find in the [SLEW Platform Tutorial](./SLEW_Platform_Tutorial.ipynb).  \n",
    "Then, enter your code snippet for result download in the following cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Access result using token\n",
    "r = Result(1, 'xyz', endpoint='https://slew.k8s.eonerc.rwth-aachen.de')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot the results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results_file_name='EMT_SynGenDQ7odTrapez_OperationalParams_SMIB_Fault_JsonSyngenParams.csv'\n",
    "\n",
    "# Get file by name\n",
    "f = r.get_file_by_name(results_file_name)\n",
    "\n",
    "# Create temp dir\n",
    "tmpdir = tempfile.mkdtemp()\n",
    "results_file_path = os.path.join(tmpdir,results_file_name)\n",
    "\n",
    "# Load files as bytes\n",
    "f = f.download(dest=results_file_path)\n",
    "\n",
    "# Get time series object from csv\n",
    "ts_res3 = read_timeseries_csv(results_file_path, print_status=False)\n",
    "\n",
    "# Plot the currents\n",
    "plt.figure(figsize=(8,12))\n",
    "name_list = ['i_gen_0', 'i_gen_1', 'i_gen_2']\n",
    "phases = ['a', 'b', 'c']\n",
    "colors = ['C0', 'C1', 'C2']\n",
    "for name in name_list:\n",
    "    i = name_list.index(name)\n",
    "    plt.subplot(3,1,1+i)\n",
    "    plt.ylabel(\"current (kA)\")\n",
    "    plt.xlabel(\"time (s)\")\n",
    "#     plt.plot(ts_res1[name].time, ts_res1[name].values/1e3, label='SG task 2 (phase '+phases[i]+')', color=colors[i])\n",
    "#     plt.plot(ts_res3[name].time, ts_res3[name].values/1e3, label='SG task 4 (phase '+phases[i]+')', color='black', linestyle=':')\n",
    "    plt.plot(ts_res3[name].time, ts_res3[name].values/1e3, label='SG task 4 (phase '+phases[i]+')', color=colors[i])\n",
    "    plt.plot(ts_res1[name].time, ts_res1[name].values/1e3, label='SG task 2 (phase '+phases[i]+')', color='0.8')\n",
    "    plt.xlim([0, 1])\n",
    "    plt.legend(loc='upper right')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "venv5",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.13.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
